<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>All in One BPO</title>

	<link rel="stylesheet" type="text/css" href="estilos/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="estilos/css/font-awesome.css">
	<!-- <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet"> -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,700,700i" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="estilos/css/estilos.css">

</head>
<body>
	<header>
		<nav id="nav_busqueda" style="background-color: #3D3C3E; height: 54px;">
			<div class="container-fluid contenedorNavBusqueda">

				<form class="navbar-form navbar-right formularioNav" role="search">
					<div class="">
						<ul class="social-top">
							<li><img src="estilos/iconos/facebook.png" alt=""/></li>
							<li><img src="estilos/iconos/twitter.png" alt=""/></li>
							<li><img src="estilos/iconos/instagram.png" alt=""/></li>
							<li><img src="estilos/iconos/googleplus.png" alt=""/></li>
							<li><img src="estilos/iconos/linkedin.png" alt=""/></li>
						</ul>
						<div class="form-control campoBusquedaGeneral">
							<input type="text" placeholder="Search...">
							<button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
						</div>
					</div>
					
				</form>
			</div>
		</nav>
		<nav class="navbar navbar-default navbar-static-top navbarPrincipal">
			<div class="container-fluid">
				<div class="container-fluid">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbarMenu" aria-expanded="false">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="#">
							<img alt="Brand" class="img-responsive imagen-logo" max-width="210px" src="estilos/iconos/logo.png">
						</a>
					</div>
					<div class="navbar-right">
						<div class="divContanctoNav">	
							<br>
							<div id="telefonoContacto">
								<img src="estilos/iconos/telefono.png" >
								+(57) 300 3751248
							</div>
							<div id="correoContacto">
								<a href="mailto:gerencia@gijutsudesigns.com" class="enlaceDefault">gerencia@gijutsudesigns.com</a>
							</div>
						</div>
					</div>

					<div  id="navbarMenu" class="navbar-collapse collapse in" aria-expanded="true">
						<ul class="nav navbar-nav">
							<li class="active"><a href="#">Inicio</a></li>
							<li><a href="#">Servicios</a></li>
							<li><a href="#">Empresa</a></li>
							<li><a href="#">Clientes</a></li>
							<li><a href="#">Contacto</a></li>
							<li><a href="#">Blog</a></li>
							<li><a href="#">Tienda</a></li>
						</ul>
					</div>

				</div>

			</div>

		</nav>
	</header>
	<article id="articlePrincipal">
		<section id="sectionCarusel">
			<div id="myCarousel" class="carousel slide" data-ride="carousel">
				<!-- Indicators -->
				<ol class="carousel-indicators">
					<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
					<li data-target="#myCarousel" data-slide-to="1"></li>
					<li data-target="#myCarousel" data-slide-to="2"></li>
					<li data-target="#myCarousel" data-slide-to="4"></li>
				</ol>

				<!-- Wrapper for slides -->
				<div class="carousel-inner">
					<div class="item active">
						<img src="estilos/imagenes/slider1.jpg" alt="1" style="width:100%;">
					</div>

					<div class="item">
						<img src="estilos/imagenes/slider2.jpg" alt="2" style="width:100%;">
					</div>

					<div class="item">
						<img src="estilos/imagenes/slider3.jpg" alt="3" style="width:100%;">
					</div>

					<div class="item">
						<img src="estilos/imagenes/slider4.jpg" alt="4" style="width:100%;">
					</div>
				</div>

				<!-- Left and right controls -->
				<a class="left carousel-control" href="#myCarousel" data-slide="prev">
					<span class="glyphicon glyphicon-chevron-left"></span>
					<span class="sr-only">Previous</span>
				</a>
				<a class="right carousel-control" href="#myCarousel" data-slide="next">
					<span class="glyphicon glyphicon-chevron-right"></span>
					<span class="sr-only">Next</span>
				</a>
			</div>
		</section>

		<!-- Nuestros servicios -->
		<section id="contenedorNuestrosServicios">
			<div class="tituloX2 tLight">
				Nuestros
			</div>
			<div class="tituloX3 tBold">
				Servicios				
			</div>
		</section>
		<!-- fin nuestros servicios -->

		<!-- listado servicios -->
		<section id="contenedorListaServicios">
			<div class="col-md-12" style="padding-bottom: 30px;">
				<!-- customer service -->
				<div class="col-md-3" >
					<div class="contenedorImagen">
						<img src="estilos/iconos/customer.png" alt="">
					</div>
					<div class="tituloDefault">Customer Service</div>
					<hr>
					<div class="contenidoItem">
						Hay dos verdades, es más caro conse-<br>
						guir un cliente nuevo que retener al<br>
						existente, y la retención del cliente está<br>
						relacionada con...<br>
					</div>
				</div>
				<!-- telemarketing -->
				<div class="col-md-3" >
					<div class="contenedorImagen">
						<img src="estilos/iconos/telemarketing.png" alt="">
					</div>
					<div class="tituloDefault">Tele Marketing</div>
					<hr>
					<div class="contenidoItem">
						En una industria tan dinámica y compe-<br>
						titiva como el Telemarketing, la rapidez<br>
						de adaptación a los cambios en las<br>
						condiciones de negocios...<br>
					</div>
				</div>
				<!-- gestion cartera -->
				<div class="col-md-3" >
					<div class="contenedorImagen">
						<img src="estilos/iconos/gestion.png" alt="">
					</div>
					<div class="tituloDefault">Gestión Cartera</div>
					<hr>
					<div class="contenidoItem">
						Actividad que permite cobrar en sus<br>
						diversas etapas, mora temprana, mora<br>
						tardía o mora prejudicial, proporcionan-<br>
						do las alertas necesarias al cliente...<br>
					</div>
				</div>
				<!-- teleencuesta -->
				<div class="col-md-3" >
					<div class="contenedorImagen">
						<img src="estilos/iconos/teleencuesta.png" alt="">
					</div>
					<div class="tituloDefault">Tele Encuesta</div>
					<hr>
					<div class="contenidoItem">
						Actividad de encuesta telefónica a<bR>
						través de un ejecutivo de teleencuesta<bR>
						capaz de brindar un rápido relevamien-<bR>
						to de información de parte de...<bR>
					</div>
				</div>
			</div>
			<center>
				<button class="boton botonRojo">VER MÁS</button>
			</center>
		</section>
		<!-- fin listado servicios -->

		<!-- quienes somos -->
		<section>
			<div class="col-md-12 noPadding">
				<!-- imagen -->
				<div class="col-md-6 noPadding">
					<img src="estilos/imagenes/img_2624.jpg" class="img-responsive">
				</div>
				<div class="col-md-6 contenedorQuienesSomos tBlanco">
					<div class="tituloX1 tBold">
						¿Quiénes Somos?		
					</div>
					<div>
						<p class="pTextoQuienesSomos">
							Organización especializada en servicios de Business Process Outsourcing<br>
							(BPO) y Contact center que provee soluciones integrales<br>
							de comunicaciones y servicios de Promoción y Ventas a compa-<br>
							ñías de diferentes sectores económicos y financieros.<br>
						</p><br>
						<p class="pTextoQuienesSomos">
							Nuestros servicios están alineados a las estrategias de nuestros<br>
							clientes contribuyendo a su rentabilidad.<br>
						</p><br>
						<button class="boton botonTransparente">VER MÁS</button>
					</div>
				</div>
			</div>
		</section>
		<!-- fin quienes somos -->

		<!-- Lineas Negocio -->
		<section id="contenedorLineasNegocio">
			<div class="tituloX2 tLight">
				Lineas de
			</div>
			<div class="tituloX3 tBold">
				Negocio				
			</div>
		</div>
	</section>
	<!-- fin Lineas Negocio -->

	<!-- listado Negocios -->
	<section id="contenedorListaNegocios">
		<div class="col-md-12" style="padding-bottom: 30px;">
			<!-- I LINEA DE NEGOCIO -->
			<div class="col-md-4 contenedorLineaNegocio" >
				<div class="contenedorImagen">
					<img src="estilos/imagenes/Ilinea1.jpg" alt="">
				</div>
				<center>
					<h2 class="tBold">I LINEA DE NEGOCIO</h2>
				</center>
				<hr>
				<div class="contenidoItem pLineaNegocio">
					La subcontratación de funciones del<br>
					proceso de negocio en proveedores de<br>
					servicios, ya sea interno o externo a la<br>
					empresa, que se suponen menos costosos<br>
					o más eficientes.<br>
				</div>
			</div>
			<div class="col-md-4 contenedorLineaNegocio" >
				<div class="contenedorImagen">
					<img src="estilos/imagenes/Ilinea2.jpg" alt="">
				</div>
				<center>
					<h2 class="tBold">II LINEA DE NEGOCIO</h2>
				</center>
				<hr>
				<div class="contenidoItem pLineaNegocio">
					Suministro en modo de Alquiler de infraes-<br>tructura
					de Call Center, Oficinas amobladas,<br>
					espacios de coworking, oficinas virtuales,<br>
					Software en nube de erp, crm, contable,<br>
					comercial y financiero<br>
				</div>
			</div>
			<div class="col-md-4 contenedorLineaNegocio" >
				<div class="contenedorImagen">
					<img src="estilos/imagenes/Ilinea3.jpg" alt="">
				</div>
				<center>
					<h2 class="tBold">III LINEA DE NEGOCIO</h2>
				</center>
				<hr>
				<div class="contenidoItem pLineaNegocio">
					Se manejan diferente tipos de campañas<br>
					para la Interacción con los clientes y prospectos<br>
					utilizando estrategias de administración<br>
				</div>
			</div>
		</div>
		<center>
			<button class="boton botonRojo">VER MÁS</button>
		</center>
	</section>
	<!-- fin listado Negocios -->

	<!-- inicio nuestros clientes -->
	<section>
		<div class="col-md-12 noPadding">
			<div class="col-md-12 noPadding">
				<div class="col-md-12 noPadding">
					<img src="estilos/imagenes/fondoQuienesSomosGrande.jpg" alt="" class="img-responsive fondoAnchoCompleto">
				</div>
				<div class="fusionSecciones">	
					<div class="tituloNuestrosClientes tBlanco">
						<div class="tituloX2">
							Nuestros
						</div>
						<div class="tituloX3 tBold">
							Clientes
						</div>
					</div>
					<div class="sliderClientes">

					</div>
					<div class="col-md-12 contenidoFunciones noPadding">
						<img src="estilos/imagenes/0000.png" alt="" class="img-responsive fusionSecciones ">
						<div class="textoFunciones">
							All in One BPO buscar brindar por medio del call center la interacción<br>
							directa con el cliente, de esta forma de primera mano se tiene las necesidades<br>
							del mismo, y se puede llegar a ofertar correctamente productos<br>
							y servicios que satisfagan su necesidad actual con un precio<br>
							razonable y estándares de calidad en la prestación del mismo.<br>
						</div>
					</div>
				</div>
			</div>	
		</div>
	</section>
	<!-- fin nuestros clientes -->

	<!-- blog -->
<!-- <section>
	<div class="col-md-12 noPadding">
		<div class="fondoBaseBlog">
			<img src="estilos/imagenes/fondoBlogGrande.jpg" alt="" class="img-responsive fondoAnchoCompleto imagenPosicionAbsoluta">
			<div class="col-md-6 tituloBlog">
				<div class="tituloX4 tBold tBlanco tituloBlog" style="padding-top: 50%;">
				    Blog		
				</div>
			</div>
		</div>
		<div class="col-md-12">
			<div class="col-md-6" ></div>
		</div>
	</div>
</section> -->
<!-- fin blog -->

<!-- formulario contacto -->
<!--<section>
	<div class="col-md-12 noPadding fondoImagenContacto">
		<form method="post" accept-charset="utf-8">
			<center>
				<div class="tBold tituloX2 tituloContactenos">
					Contáctenos				
				</div><br>
				<div class="descripcionContactenos">
					Para cualquier duda y/o inquietud, envíenos un mensaje y nosotros lo estaremos comunicando
				</div>
				<table cellspacing="10">
					<tbody>
						<tr>
							<td>
							<input type="text" name="nombre" placeholder="Nombre"  class="campoFormularioContacto">&nbsp; 
							</td>
							<td>
							<input type="text" name="correo" placeholder="Correo electronico" class="campoFormularioContacto">&nbsp;
							</td>
							<td>
							<input type="text" name="asunto" placeholder="Asunto" class="campoFormularioContacto">&nbsp;
							</td>
						</tr>
						<tr>
							<td colspan="3">
								<textarea name="" rows="7" class="campoTextAreaFormulario"></textarea>						
							</td>
						</tr>
					</tbody>
				</table>
				<button type="submit" class="btn botonRojo">Enviar</button>
				<div class="container formContacto" style="margin-bottom: 500px;">
					<div>
					</div><br>
					<div class="container">
					</div>
				</div> 
			</center>

			
		</form>
	</div>
</section>-->
<!-- fin formulario contacto -->

</article><!-- /article -->
<footer id="pieceraPagina">

</footer><!-- /footer -->

<script src="estilos/js/jquery-2.2.3.min.js" ></script>
<script src="estilos/js/bootstrap.js" ></script>
</body>
</html>